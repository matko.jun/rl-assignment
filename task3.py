# -*- coding: utf-8 -*-

import datetime
from time import sleep


class Cached:
    """
    Simple caching decorator which stores the result of a function call and
    returns the cached version in subsequent calls (with the same parameters)
    for 5 minutes, or ten times ­­ whichever comes first.
    """

    CACHE_VALIDITY_PERIOD = datetime.timedelta(minutes=5)
    CACHE_INVALIDATION_CALL_COUNT = 10

    def __init__(self, fn):
        self.fn = fn
        self.cache = {}

    def __call__(self, *args, **kwargs):
        """Try to get value from cache for each function call."""
        return self.get_from_cache(*args, **kwargs)

    def get_cache_key(self, *args, **kwargs):
        """
        Get cache key based on function positional and keyword arguments.

        Note that this will work just fine for primitive arguments but might
        yield unexpected results for objects or collections.
        """
        return hash(str(args) + str(kwargs))

    def cache_and_return(self, *args, **kwargs):
        """Call function, cache return value and return it."""
        cache_key = self.get_cache_key(*args, **kwargs)
        self.cache[cache_key] = {
            'call_count': 1,
            'valid_until': datetime.datetime.now() + self.CACHE_VALIDITY_PERIOD,
            'value': self.fn(*args, **kwargs)
        }
        return self.cache[cache_key]['value']

    def get_from_cache(self, *args, **kwargs):
        """Get value from cache or refresh cached value."""
        cache_key = self.get_cache_key(*args, **kwargs)

        # we've called this function before, let's check the cache
        if cache_key in self.cache:
            now = datetime.datetime.now()
            valid_until = self.cache[cache_key]['valid_until']
            call_count = self.cache[cache_key]['call_count']

            # check for cache conditions and act accordingly
            if (now <= valid_until and
                    call_count < self.CACHE_INVALIDATION_CALL_COUNT):
                self.cache[cache_key]['call_count'] += 1
                return self.cache[cache_key]['value']

        # this is a new function call or cache expired
        return self.cache_and_return(*args, **kwargs)


if __name__ == "__main__":

    @Cached
    def get_current_timestamp(days_offset=0):
        """
        Get current timestamp with optional future offset in days as ISO
        formatted timestamp string.

        Function call notification message is printed to notify user whether
        this function was actually evaluated or we just got value from cache.
        """

        print("`get_current_timestamp` invoked!")
        current_timestamp = datetime.datetime.now()
        if days_offset:
            current_timestamp += datetime.timedelta(days=days_offset)
        return current_timestamp.isoformat()


    # TEST1: call more than 10 times with different arguments
    print(
        "==================================================================\n"
        "TEST1: Invoke function 20 times with 3 different arguments:\n"
        "==================================================================\n")

    TEST1_CALL_REPETITIONS = 20
    for repetition in range(TEST1_CALL_REPETITIONS):
        for days_offset in range(3):
            print("Call #{0}; days_offset={1}; Value: {2}".format(
                repetition, days_offset, get_current_timestamp(days_offset)
            ))

    # TEST2: call less than 10 times spanning more than 5 minutes
    print(
        "\n==================================================================\n"
        "TEST2: Invoke `get_current_timestamp` 6 times with 1 minute pause\n"
        "==================================================================\n")

    TEST2_CALL_REPETITIONS = 6
    for repetition in range(TEST2_CALL_REPETITIONS):
        now = datetime.datetime.now().isoformat()
        print("Call #{0}; Current time: {1}; Value: {2}".format(
                repetition, now, get_current_timestamp()
        ))
        # don't wait after last call
        if repetition < TEST2_CALL_REPETITIONS:
            sleep(60)
