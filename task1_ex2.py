# -*- coding: utf-8 -*-

import getopt
import os
import sqlite3
import sys


class NamesTempDb:
    """
    Simple wrapper around sqlite3 db to provide a nicer interface for our
    file processing later.
    """

    _DB_NAME = 'temp~'
    _TABLE_NAME = 'processed_data'
    _DROP_TABLE_SQL = "DROP TABLE IF EXISTS {}".format(_TABLE_NAME)
    _CREATE_TABLE_SQL = """CREATE TABLE {} (id INTEGER PRIMARY KEY, 
        first_name TEXT, last_name TEXT)""".format(_TABLE_NAME)
    _INSERT_FIRST_NAME_SQL = """INSERT INTO {} (id, first_name) 
        VALUES (?, ?)""".format(_TABLE_NAME)
    _UPDATE_LAST_NAME_SQL = """
        UPDATE {} SET last_name=? WHERE id=?""".format(_TABLE_NAME)
    _GET_ALL_ITEMS_SQL = """SELECT id, first_name, last_name FROM {} 
        ORDER BY id""".format(_TABLE_NAME)

    conn = None

    def __init__(self):
        """Open connection and start with a fresh (empty) table."""
        self.open()
        self.recreate_table()

    def open(self):
        """Open connection if it's not opened already."""
        if not self.conn:
            self.conn = sqlite3.connect(self._DB_NAME)

    def close(self):
        """Close connection if it's not opened already and delete db file."""
        if self.conn:
            self.conn.close()
            os.remove(self._DB_NAME)

    def add_first_name(self, id, first_name):
        """Add id and first_name to database."""
        c = self.get_cursor()
        c.execute(self._INSERT_FIRST_NAME_SQL, (id, first_name))
        self.commit()  # avoid exceeding `SQLITE_MAX_SQL_LENGTH` limit

    def add_last_name(self, id, last_name):
        """Add id and last_name to database."""
        c = self.get_cursor()
        c.execute(self._UPDATE_LAST_NAME_SQL, (last_name, id))
        self.commit()  # avoid exceeding `SQLITE_MAX_SQL_LENGTH` limit

    def get_cursor(self):
        """Open cursor while making sure connection is open."""
        self.open()
        return self.conn.cursor()

    def commit(self):
        """Commit all executed statements."""
        self.conn.commit()

    def recreate_table(self):
        """Drop table if exists and create new one."""
        c = self.get_cursor()
        c.execute(self._DROP_TABLE_SQL)
        c.execute(self._CREATE_TABLE_SQL)
        self.commit()

    def items(self):
        """Get all items as iterator."""
        c = self.get_cursor()
        return c.execute(self._GET_ALL_ITEMS_SQL)


def combine_files(first_names_file, last_names_file, output_file):
    """
    Read `first_names_file` and `last_names_file` to sqlite database file and
    write combined data to the `output_file`.

    We are leveraging sqlite data to tackle two issues:
     - storage of indexed data
     - storage of data larger than available memory

    By storing data to the sqlite db file we don't have to worry about running
    out of RAM. There is a trade-off however - we need extra disk space and we
    are introducing processing overhead - creation of database, writing and
    updating row, indexing and querying for output data.

    Extension #2: `output_file` contents are sorted by identifier
    """

    processed_data = NamesTempDb()

    # EX2: data is inserted to db for later processing
    with open(first_names_file, 'r') as fn:
        for line in fn:
            first_name, _, identifier = line.replace('\n', '').rpartition(' ')
            processed_data.add_first_name(int(identifier), first_name)

    with open(last_names_file, 'r') as ln:
        for line in ln:
            last_name, _, identifier = line.replace('\n', '').rpartition(' ')
            processed_data.add_last_name(int(identifier), last_name)

    with open(output_file, 'w') as of:
        # EX2: all items are read from db and written to output_file (sorted)
        for identifier, first_name, last_name in processed_data.items():
            of.write(
                '{0} {1} {2}\n'.format(
                    first_name,
                    last_name,
                    identifier))


def print_help_text():
    print(
        '{} -f <first_names_file> -l <last_names_file> -o <output_file>'
        .format(sys.argv[0])
    )


if __name__ == "__main__":
    # parse arguments
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hf:l:o:",
            ["first-names=", "last-names=", "output="])
    except getopt.GetoptError:
        print_help_text()
        sys.exit(2)

    # process arguments
    first_names_file = None
    last_names_file = None
    output_file = None

    for opt, arg in opts:
        if opt == '-h':
            print_help_text()
            sys.exit()
        elif opt in ("-f", "--first-names"):
            first_names_file = arg
        elif opt in ("-l", "--last-names"):
            last_names_file = arg
        elif opt in ("-o", "--output"):
            output_file = arg

    if not first_names_file or not last_names_file or not output_file:
        print_help_text()
        sys.exit(2)

    combine_files(first_names_file, last_names_file, output_file)
