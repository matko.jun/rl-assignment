# -*- coding: utf-8 -*-
import sys


def brackets_balanced(text):
    """
    Given the input text check if all brackets (“()”, “{}”, “[]”) are balanced.
    """

    bracket_mapping = {
        ')': '(',
        ']': '[',
        '}': '{'
    }
    open_brackets = bracket_mapping.values()
    closed_brackets = bracket_mapping.keys()

    # let's fail fast - text starts with closing bracket or ends with
    # opening bracket -> brackets are not balanced
    if text[0] in closed_brackets or text[-1] in open_brackets:
        return False

    # process given text iterating over each character and fail as soon as we
    # encounter unbalanced bracket pair.
    open_bracket_stack = []
    for character in text:
        if character in open_brackets:
            open_bracket_stack.append(character)
        if character in closed_brackets:
            if not open_bracket_stack or open_bracket_stack.pop() != bracket_mapping[character]:
                return False

    # after processing we should end with empty stack
    if open_bracket_stack:
        return False

    return True


if __name__ == "__main__":
    text = input('Text: ')

    if not text:
        print("Please provide input text.")
        sys.exit(2)

    if brackets_balanced(text):
        print("Brackets are balanced.")
    else:
        print("Brackets are NOT balanced.")
