# -*- coding: utf-8 -*-

import getopt
import sys
from collections import OrderedDict


def combine_files(first_names_file, last_names_file, output_file):
    """
    Read `first_names_file` and `last_names_file` to memory and write combined
    data to the `output_file`.

    We are leveraging `OrderedDict` here to tackle two issues:
     - efficiently correlate data by a common value (id)
     - preserve ordering while processing files

    Note that opening large files is not a problem here as we are intentionally
    avoiding file 'slurping'. However, storing data from both files in
    OrderedDict might use all of the available memory and cause this script to
    hang (or worse!).
    """

    processed_data = OrderedDict()

    with open(first_names_file, 'r') as fn:
        for line in fn:
            first_name, _, identifier = line.replace('\n', '').rpartition(' ')
            processed_data[identifier] = {'first_name': first_name}

    with open(last_names_file, 'r') as ln:
        for line in ln:
            last_name, _, identifier = line.replace('\n', '').rpartition(' ')
            processed_data[identifier]['last_name'] = last_name

    with open(output_file, 'w') as of:
        for identifier, full_name in processed_data.items():
            of.write(
                '{0} {1} {2}\n'.format(
                    full_name['first_name'],
                    full_name['last_name'],
                    identifier))


def print_help_text():
    print(
        '{} -f <first_names_file> -l <last_names_file> -o <output_file>'
        .format(sys.argv[0])
    )


if __name__ == "__main__":
    # parse arguments
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hf:l:o:",
            ["first-names=", "last-names=", "output="])
    except getopt.GetoptError:
        print_help_text()
        sys.exit(2)

    # process arguments
    first_names_file = None
    last_names_file = None
    output_file = None

    for opt, arg in opts:
        if opt == '-h':
            print_help_text()
            sys.exit()
        elif opt in ("-f", "--first-names"):
            first_names_file = arg
        elif opt in ("-l", "--last-names"):
            last_names_file = arg
        elif opt in ("-o", "--output"):
            output_file = arg

    if not first_names_file or not last_names_file or not output_file:
        print_help_text()
        sys.exit(2)

    combine_files(first_names_file, last_names_file, output_file)
